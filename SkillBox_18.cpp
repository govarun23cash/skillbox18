﻿// SkillBox_18.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

//#include "stdafx.h"
#include <iostream>
using namespace std;
//int main()


using namespace std;

#include <iomanip>

template <typename A>
class Stack
{
private:
    A* stackPtr; // указатель на стек
    int size; // размер стека
    A top; // вершина стека
public:
    Stack(int = 10);// по умолчанию размер стека равен 10 элементам
    ~Stack(); // деструктор
    bool push(const A); // поместить элемент в стек
    bool pop(); // удалить из стека элемент
    void printStack();
};

int main()
{   Stack <int> myStack(5);
    setlocale(LC_ALL, "Russian");
    // заполняем стек
    cout << "Заполняем стек: ";
    int ct = 0;
    while (ct++ != 5)
    {
        int temp;
        cin >> temp;
        myStack.push(temp);
    }

    myStack.printStack(); // вывод стека на экран
    setlocale(LC_ALL, "Russian");

    cout << "\nУдаляем первый элемент из стека:\n";

    myStack.pop(); // удаляем элемент из стека
    myStack.printStack(); // вывод стека на экран

    return 0;
}

// конструктор
template <typename A>
Stack<A>::Stack(int s)
{
    size = s > 0 ? s : 10;   // инициализировать размер стека
    stackPtr = new A[size]; // выделить память под стек
    top = -1; // значение -1 говорит о том, что стек пуст
}

// деструктор
template <typename A>
Stack<A>::~Stack()
{
    delete[] stackPtr; // удаляем стек
}

// элемент функция класса  Stack для помещения элемента в стек
// возвращаемое значение - true, операция успешно завершена
//                                    false, элемент в стек не добавлен
template <typename A>
bool Stack<A>::push(const A value)
{
    if (top == size - 1)
        return false; // стек полон

    top++;
    stackPtr[top] = value; // помещаем элемент в стек

    return true; // успешное выполнение операции
}

// элемент функция класса  Stack для удаления элемента из стек
// возвращаемое значение - true, операция успешно завершена
//                                    false, стек пуст
template <typename A>
bool Stack<A>::pop()
{
    if (top == -1)
        return false; // стек пуст

    stackPtr[top] = 0; // удаляем элемент из стека
    top--;

    return true; // успешное выполнение операции
}

// вывод стека на экран
template <typename A>
void Stack<A>::printStack()
{
    for (int ix = size - 1; ix >= 0; ix--)
        cout << "|" << setw(4) << stackPtr[ix] << endl;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
